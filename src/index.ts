export function compare(x: Object, y: Object) {
	if (typeof x !== 'object') {
		throw Error('x is not an object!');
	}

	if (typeof y !== 'object') {
		throw Error('y is not an object!');
	}

	return JSON.stringify(x) === JSON.stringify(y);
}

export function uniqueArray(x: Array<any>) {
	return [ ...new Set(x) ];
}

export function minValArray(x: Array<number>) {
	if (!Array.isArray(x)) {
		throw new Error('You need to pass an array of numbers.');
	}

	return Math.min.apply(null, x);
}

export function maxValArray(x: Array<number>) {
	if (!Array.isArray(x)) {
		throw new Error('You need to pass an array of numbers.');
	}

	return Math.max.apply(null, x);
}

export function isInt(x: any) {
	if (x == null) {
		return false;
	}

	return x % 1 === 0;
}

export function randomInt(min: number, max: number, maxIncluded: boolean = false) {
	if (!isInt(min)) {
		throw Error('min is not an integer');
	}

	if (!isInt(max)) {
		throw Error('max is not an integer');
	}

	if (maxIncluded) {
		return Math.floor(Math.random() * (max - min + 1)) + min;
	}

	return Math.floor(Math.random() * (max - min)) + min;
}

export function cacheFunc(func: Function, showLog = false) {
	if (typeof func !== 'function') {
		throw new Error('You need to pass a function.');
	}

	let cache: any = {};

	return function(...rest: any) {
		if (rest.length === 0) {
			throw new Error('In order to be cached function needs at least one parameter.');
		}

		let key = '';
		rest.forEach((k: any) => {
			key += JSON.stringify(k);
		});
		if (cache[key]) {
			if (showLog) {
				console.log('cache');
			}

			return cache[key];
		} else {
			if (showLog) {
				console.log('no cache');
			}
			let res = func(...rest);
			cache[key] = res;
			return res;
		}
	};
}

export function timeSpent(callback: Function, showLog: boolean = true) {
	if (typeof callback !== 'function') {
		throw new Error('You need to pass a function.');
	}

	return function(...rest: any) {
		if (showLog) {
			console.time(callback.name);
		}
		callback(...rest);
		if (showLog) {
			console.timeEnd(callback.name);
		}
	};
}
